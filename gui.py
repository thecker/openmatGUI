# This file is part of openmatGUI
#
# openmatGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with openmatGUI.  If not, see <https://www.gnu.org/licenses/>.

from desktop_simple import Ui_MainWindow
from PyQt5.Qt import QApplication, QMainWindow, QLabel, QLineEdit, QWidget, QTextBrowser, QTreeWidgetItem, \
    QTableWidgetItem
import sys
from openmatdb.pyopenmatdb import OpenmatEnvironment
import pprint
import matplotlib.pyplot as plt
import warnings
import json


class GUI(QMainWindow, Ui_MainWindow):

    def __init__(self, openmat_env):

        super().__init__()

        self.openmat_env = openmat_env
        self.plugin_widgets = []
        self.plugin_params = []
        self.active_plugin_no = 0
        self.results_count = 0
        self.pp = pprint.PrettyPrinter(indent=4)

        # init UI elements
        self.setupUi(self)
        self.update_plugin_list()
        self.update_input_UIs()
        self.update_tree_view()
        self.init_navi_table()

        # define actions
        self.lV_plugins.itemSelectionChanged.connect(self.update_input_UIs)
        self.butRun.clicked.connect(self.update_plugin_params)
        self.tabWidget.tabCloseRequested.connect(self.close_tab)
        self.treeNavigator.itemClicked.connect(self.update_navi_table)
        self.tabWidget.tabBarClicked.connect(self.update_tabs)     # to update collection / record ID for
        # actions tab after a new record is selected in navigator

        self.show()

    def update_plugin_list(self):
        """Adds the registered plugins to list of plugins"""
        plugin_names = [plugin['name'] for plugin in self.openmat_env.plugin_config]
        # print(plugin_names)
        self.lV_plugins.addItems(plugin_names)

    def update_tabs(self, index):
        """Updates the tabs"""
        if index == 0:
            self.update_tree_view()
        if index == 1:
            self.update_input_UIs()

    def update_input_UIs(self):
        """Updates the UI elements required for the current plugin"""

        plugin_index = self.lV_plugins.selectedIndexes()

        if plugin_index in [None, []]:
            self.active_plugin_no = 0
        else:
            self.active_plugin_no = plugin_index[0].row()

        for i in range(self.layout_inputs.rowCount()):
            self.layout_inputs.removeRow(0)

        # for widget in self.plugin_widgets:
        #     del widget
        self.plugin_widgets = []

        for UI_element in self.openmat_env.plugin_config[self.active_plugin_no]['UI_elements']:
            new_label = QLabel(UI_element['label'])
            if UI_element['type'] == 'TextInput':
                # TODO: this could be improved (or must at least be explained in the documentation)...
                if UI_element['label'] == 'Collection':
                    new_widget = QLineEdit(self.openmat_env.active_collection)
                elif UI_element['label'] == 'Record ID':
                    new_widget = QLineEdit(self.openmat_env.active_record_id)
                elif UI_element['label'] == 'UI type':
                    new_widget = QLineEdit('desktop')
                else:
                    new_widget = QLineEdit(UI_element['default_value'])
            self.plugin_widgets.append(new_widget)
            self.layout_inputs.addRow(new_label, new_widget)

        self.update_plugin_help()

    def update_plugin_help(self):
        """Updates the displayed help text"""
        try:
            help_str = self.openmat_env.plugin_config[self.active_plugin_no]['help']
        except KeyError:
            help_str = 'No help available.'

        try:
            author = self.openmat_env.plugin_config[self.active_plugin_no]['author']
        except KeyError:
            author = 'Unknown'

        try:
            copy_right = self.openmat_env.plugin_config[self.active_plugin_no]['copyright']
        except KeyError:
            copy_right = 'Unknown'

        try:
            lic = self.openmat_env.plugin_config[self.active_plugin_no]['license']
        except KeyError:
            lic = 'Unknown'

        full_str = "{} ({})\n" \
                   "\n" \
                   "{}\n\n" \
                   "Author(s): {}\n" \
                   "Copyright: {}\n" \
                   "License: {}".format(self.openmat_env.plugin_config[self.active_plugin_no]['name'],
                                        self.openmat_env.plugin_config[self.active_plugin_no]['filename'],
                                        help_str, author, copy_right, lic)

        self.lblPluginHelp.setText(full_str)

    def update_plugin_params(self):
        """Reads the plugin parameters from the GUI elements"""
        self.plugin_params = []
        for i, widget in enumerate(self.plugin_widgets):
            try:
                plugin_param_type = self.openmat_env.plugin_config[self.active_plugin_no]['UI_elements'][i]['data_type']
            except KeyError:
                plugin_param_type = 'str'   # assume str if data type is not defined
                warnings.warn('No data type defined for {} - {} - assuming \"str\"'.format(
                    self.openmat_env.plugin_config[self.active_plugin_no]['name'],
                    self.openmat_env.plugin_config[self.active_plugin_no]['UI_elements'][i]['label']))
            if type(widget) == QLineEdit:
                if plugin_param_type == 'str':
                    self.plugin_params.append(widget.text())
                else:
                    self.plugin_params.append(eval(widget.text()))
        print("Plug-in params:", self.plugin_params)

        self.run_plugin()

    def run_plugin(self):
        """Launches a plug-in"""

        active_plugin_name = self.openmat_env.plugin_config[self.active_plugin_no]['module']

        result = self.openmat_env.run_plugin(active_plugin_name, *self.plugin_params)

        try:
            return_type = self.openmat_env.plugin_config[self.active_plugin_no]['return_type']
        except KeyError:
            return_type = None

        self.create_results_tab(result, return_type)

    def create_results_tab(self, result, return_type):
        """Creates a new results tab"""

        self.results_count += 1

        tabResults = QWidget()
        tabResults.setEnabled(True)
        tabResults.setObjectName("Results" + str(self.results_count))

        if return_type is None or return_type == 'text':
            json_str = json.dumps(result, indent=4, default=str)
            results_widget = QTextBrowser()
            results_widget.setText(json_str)

        elif return_type == 'pyplot':
            plt.show()
        elif return_type == 'table':
            pass
        else:
            warnings.warn('Plug-in defined unknown return type {}.'.format(return_type))

        try:
            tabname = "Results" + str(self.results_count)
            self.tabWidget.addTab(results_widget, tabname)
            self.tabWidget.setCurrentWidget(results_widget)
        except UnboundLocalError:
            pass

    def update_tree_view(self):
        """Updates the tree view of the DB"""

        self.treeNavigator.clear()
        db_tree = self.openmat_env.run_plugin('get_tree')

        top_items = db_tree.keys()
        top_tree_items = []
        for item in top_items:
            new_item = QTreeWidgetItem([item])
            top_tree_items.append(new_item)
            for record_id in db_tree[item]:
                QTreeWidgetItem(new_item, [record_id])

        self.treeNavigator.addTopLevelItems(top_tree_items)

    def fill_table(self, table_widget, record_dict):
        """Clears and fills a with a new record dictionary"""

        # clear
        table_widget.clearContents()
        for i in range(table_widget.rowCount()):
            table_widget.removeRow(0)

        for i, key in enumerate(record_dict.keys()):
            key_item = QTableWidgetItem(key)
            value_item = QTableWidgetItem(str(record_dict[key]))
            table_widget.insertRow(i)
            table_widget.setItem(i, 0, key_item)
            table_widget.setItem(i, 1, value_item)

    def init_navi_table(self):
        """Initialize the header names in the navi table"""
        header1 = QTableWidgetItem("Attribute")
        header2 = QTableWidgetItem("Value")

        self.tableNavigator.setHorizontalHeaderItem(0, header1)
        self.tableNavigator.setHorizontalHeaderItem(1, header2)

    def update_navi_table(self, selected_item, column):
        """Updates the table in the navigator view"""

        #selected_items = self.treeNavigator.selectedItems()[0]
        record_id = selected_item.text(column)
        collection = selected_item.parent()
        if collection is None:  # the selected_item is a top level item - i.e. a collection
            # self.openmat_env.active_record_id = None
            self.openmat_env.active_collection = record_id
        else:
            collection_name = collection.text(0)
            record = self.openmat_env.run_plugin('record_by_ID', *[collection_name, record_id])
            self.fill_table(self.tableNavigator, record)

            # update active record ID and collection name in DB session
            self.openmat_env.active_collection = collection_name
            self.openmat_env.active_record_id = record_id
        print('Active collection = {}'.format(self.openmat_env.active_collection))
        print('Active record_id = {}'.format(self.openmat_env.active_record_id))


    def close_tab(self, index):
        """Closes the current tab (mandatory tabs cannot be closed)"""
        if index >= 2:
            self.tabWidget.removeTab(index)


if __name__ == '__main__':
    app = QApplication(sys.argv)

    # api_url = 'http://0.0.0.0:5000/v1'
    openmat_env = OpenmatEnvironment()

    # add python plugin-server #1
    py_plugin_server_url = 'http://172.21.0.1:5020'
    openmat_env.add_plugin_server(py_plugin_server_url)

    # add python plugin-server #2
    py_plugin_server_url = 'http://172.21.0.1:5030'
    openmat_env.add_plugin_server(py_plugin_server_url)

    gui = GUI(openmat_env)

    # update the plug-in list UI element

    sys.exit(app.exec_())
