About
=====

*openmatGUI* is a desktop GUI for *openmatDB*.

*openmatGUI* is free software released under the GNU GENERAL PUBLIC LICENSE Version 3 (GPLv3).

See NOTICE and LICENSE file for details.

*openmatGUI* is written in python and depends on following packages:

* Python3
* PyQt5
* matplotlib
* pyopenmatdb

Please see the documentation for more details on *openmatDB* and its components:
https://openmatDB.readthedocs.io/en/latest/
